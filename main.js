const mongoose = require('mongoose');
const connection = require('./connection');
const { User } = require('./schema');

/**
 * Uses mongo aggregation to copy records from one collection to 
 * archived collection with $out
 * removes archived records from collection
 */

function connect() {
    return new Promise((res, rej) => {
        mongoose.connect(connection, {
            useUnifiedTopology: true,
            useNewUrlParser: true
        }).then(res).catch(e => {
            console.log(e)
            rej(e);
        });
    });
}

async function insert() {
    await connect()
        .then(async (connection) => {
            for (let i = 0; i < 1000; i++) {
                try {
                    const array = Array.from(new Array(9999)).map((i, index) => {
                        return new User({
                            name: "Someone"+index,
                            active: index % 12 === 0
                        });
                    });
                    const s = await User.insertMany(array);
                } catch (error) {
                    console.log(error);
                }
            }
        })
        .catch(e => {
            console.log(e);
            throw e;
        })
}

async function query() {
    const response = await User.aggregate([
        {
            $match: {
                active: false
            }
        }, {
            $out: 'archived'
        }
    ]);
    return response;
}

async function remove() {
    const records = await User.deleteMany({ active: false }).exec();
    return records;
}

// get operation from cli arguements
let operation = process.argv[2];

// remove records from collection
if (operation === "remove") {
    console.time('remove');
    remove()
        .then(() => {
            console.timeEnd('remove');
        })
        .catch(e => {
            console.log('ERROR:', e);
        });

}


// copy records to archived collection
if (operation === "query") {
    console.time('query');
    query()
        .then(() => {
            console.timeEnd('query');
        })
        .catch((e) => {
            console.log('ERROR:', e);
        })
}



// insert records into collection
if (operation === "insert") {
    console.time('insert');
    insert()
        .then(() => {
            console.timeEnd('insert');
        })
        .catch((e) => {
            console.log('ERROR:', e);
        });
}