package main

import (
	"context"
	"fmt"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// Demo struct
type Demo struct {
	Name   string `bson:"name"`
	Active bool   `bson:"active"`
}

const (
	mongoURI = "mongodb://username:password@localhost:27017/?authSource=admin"
)

func main() {
	start := time.Now()
	client, err := mongo.NewClient(options.Client().ApplyURI(mongoURI))
	if err != nil {
		log.Fatal(err)
	}
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	err = client.Connect(ctx)
	if err != nil {
		log.Fatal(err)
	}

	db := client.Database("testdb")

	var myArray []interface{}

	for j := 0; j < 1000; j++ {
		myArray = []interface{}{}
		for i := 0; i < 100; i++ {
			myArray = append(myArray, Demo{
				Name:   "Someone",
				Active: i%12 == 0,
			})
			fmt.Printf("Inserted records %d:%d\n", j, i)
		}
		// insert into collection
		db.Collection("archived").InsertMany(ctx, myArray)
	}
	fmt.Printf("%s took %v\n", "operation", time.Since(start))
	defer client.Disconnect(ctx)
}
