const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  name: String,
  active: Boolean
});

exports.User = mongoose.model('Users', UserSchema);