let mongodb = require('mongodb');
const connection = require('./connection');
const cluster = require('cluster');
const os = require('os');

/**
 * Uses cluster module to run insert and remove bulkOperations in parallel
 */


// limit records for each worker
const BATCH_SIZE = 400_000;

function run() {
    const cpus = os.cpus();
    const numCpus = cpus.length;

    if (cluster.isMaster) {
        for (let i = 0; i < numCpus ; i++) {
            cluster.fork();
        }
        cluster.on("exit", (worker, code, signal) => {
            console.log(`worker exited ${worker.process.pid} ${signal.length} ${code}`)
        })
    } else {
        let workerId = cluster.worker.id
        console.time(`worker-${workerId}`)
        execute(workerId, function (records) {
            console.timeEnd(`worker-${workerId}`);
            console.log(`worker:${workerId} finished execution on ${records} documents`);
        });
    }
}

// create bulk insert operation in collection
async function insertBulkOperation(workerId, collection, documents) {
    let bulkInsert = collection.initializeUnorderedBulkOp();
    let insertedIds = [];
    let id;
    await documents.forEach(function (doc) {
        id = doc._id;
        bulkInsert.find({ _id: id }).upsert().replaceOne(doc);
        insertedIds.push(id);
    });
    console.log(`worker ${workerId} performing operations on ${insertedIds.length} records`);
    await bulkInsert.execute();
    return insertedIds;
}



// create bulk remove in collection
async function deleteBulkOperation(collection, idsToRemove) {
    let bulkRemove = collection.initializeUnorderedBulkOp();
    bulkRemove.find({
        _id: {
            $in: idsToRemove
        }
    }).remove();
    await bulkRemove.execute();
}

async function moveDocuments(workerId, sourceCollection, targetCollection, filter, batchSize) {
    let sourceDocs = await sourceCollection
        .find(filter)
        .skip((workerId - 1) * batchSize)
        .limit(batchSize);

    let idsOfCopiedDocs = await insertBulkOperation(workerId, targetCollection, sourceDocs);
    await deleteBulkOperation(sourceCollection, idsOfCopiedDocs);
    return idsOfCopiedDocs.length;
}

async function execute(workerId, callback) {
    mongodb.connect(connection, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }).then((conn) => {
        let database = conn.db('testdb')
        const sourceCollection = database.collection('testCollection');
        const targetCollection = database.collection('archivedCollection');
        moveDocuments(workerId, sourceCollection, targetCollection, { active: false }, BATCH_SIZE ).then(v => {
            callback(v);
        })
    });
}

run();